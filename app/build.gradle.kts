plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("com.google.dagger.hilt.android")
}

android {
    namespace = "ae.gov.fta.maskan"
    compileSdk = 33

    defaultConfig {
        applicationId = "ae.gov.fta.maskan"
        minSdk = 26
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildFeatures {
        viewBinding = true
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
}

dependencies {

    implementation(Deps.kotlin)
    implementation(Deps.appCompat)
    implementation(Deps.materialDesign)
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.6.1")
    testImplementation(project(":app"))
    implementation(Deps.timber)
    implementation(Deps.constraintLayout)
    implementation(Deps.coroutines)

    // testing dependencies
    testImplementation(Deps.coroutinesTest)
    androidTestImplementation(Deps.coroutinesTest)
    testImplementation(Deps.mockServer)
    testImplementation(Deps.truth)
    testImplementation(Deps.junit)
    kaptTest(Deps.hiltTest)
    androidTestImplementation(Deps.junitInst)
    androidTestImplementation(Deps.espresso)
    androidTestImplementation(Deps.truth)
    kaptAndroidTest(Deps.hiltTest)
    testImplementation(Deps.hiltTest)
    testImplementation(Deps.mockk)
    //hilt for DI
    implementation(Deps.hilt)
    kapt(Deps.hiltCompiler)

    //Retrofit
    implementation(Deps.retrofit)
    implementation(Deps.gson)
    implementation(Deps.logginfInterceptor)

    implementation(platform("org.jetbrains.kotlin:kotlin-bom:1.8.0"))
    implementation("androidx.fragment:fragment-ktx:1.6.1")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.2")
    testImplementation ("androidx.arch.core:core-testing:2.2.0")
    testImplementation ("com.android.support.test:runner:1.0.2")
}

