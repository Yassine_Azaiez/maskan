package ae.gov.fta.maskan.core.di

import ae.gov.fta.maskan.core.utils.networkUtils.MaskanApiClient
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {


    @Singleton
    @Provides
    fun provideHttpClient(cache: Cache): OkHttpClient =
        MaskanApiClient.providesOkHttpClient(cache)

    @Singleton
    @Provides
    fun provideCache(@ApplicationContext context: Context) =
        MaskanApiClient.providesOkHttpCache(context)

    @Singleton
    @Provides
    fun providRetrofit(httpClient: OkHttpClient) =
        MaskanApiClient.providesRetrofitInstance(httpClient)


}