package com.example.homehero.core.utils.baseUI.fragment

import ae.gov.fta.maskan.core.baseUI.fragment.BaseFragment
import ae.gov.fta.maskan.core.baseUI.fragment.Inflate
import ae.gov.fta.maskan.core.baseUI.viewmodel.BaseViewModel
import android.os.Bundle
import androidx.viewbinding.ViewBinding

abstract class BaseViewModelFragment<B : ViewBinding>(inflate: Inflate<B>) :
    BaseFragment<B>(inflate) {

    abstract val viewModel: BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }



}