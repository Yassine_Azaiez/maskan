package ae.gov.fta.maskan.core.utils

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.GsonBuilder

import javax.inject.Singleton

@Singleton
class AppSharedPreferences(context: Context) {


    private val prefs: Lazy<SharedPreferences> = lazy {
        context.applicationContext.getSharedPreferences(
            "skoove_preefs", Context.MODE_PRIVATE
        )
    }
    private val gson = GsonBuilder().create()


    var sessionId: String
        get() = prefs.value.getString("test", "") ?: ""
        set(value) = prefs.value.edit { putString("test", value) }
}