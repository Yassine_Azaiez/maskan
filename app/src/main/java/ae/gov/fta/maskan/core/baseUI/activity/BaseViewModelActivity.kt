package com.example.homehero.core.utils.baseUI.activity

import ae.gov.fta.maskan.core.baseUI.activity.BaseActivity
import ae.gov.fta.maskan.core.baseUI.fragment.Inflate
import ae.gov.fta.maskan.core.baseUI.viewmodel.BaseViewModel
import android.os.Bundle
import androidx.viewbinding.ViewBinding

abstract class BaseViewModelActivity<B : ViewBinding, VM : BaseViewModel>(inflate: Inflate<B>) :
    BaseActivity<B>(inflate) {

    abstract val viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

}