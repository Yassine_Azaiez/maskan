package ae.gov.fta.maskan.core.di


import ae.gov.fta.maskan.core.utils.AppSharedPreferences
import ae.gov.fta.maskan.data.remote.MaskanAppApi
import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object CoreModule {

    @Provides
    @Singleton
    fun provideRandomWordApiService(retrofit: Retrofit): MaskanAppApi =
        retrofit.create(MaskanAppApi::class.java)


    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Singleton
    fun providesSharedPreferences(@ApplicationContext context: Context): AppSharedPreferences =
        AppSharedPreferences(context)
}