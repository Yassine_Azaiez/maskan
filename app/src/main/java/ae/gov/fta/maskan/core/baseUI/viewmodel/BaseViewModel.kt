package ae.gov.fta.maskan.core.baseUI.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

open class BaseViewModel(
) : ViewModel() {

    protected val _toggleLoading = MutableStateFlow(false)
    val toggleLoading = _toggleLoading.asStateFlow()

    private suspend fun showLoadingToggle(visible: Boolean) = _toggleLoading.emit(visible)



    fun clearViewModel() {
        this.onCleared()
    }

}