package ae.gov.fta.maskan.core.di

import ae.gov.fta.maskan.data.dataSource.MaskanDataSource
import ae.gov.fta.maskan.data.dataSource.MaskanDataSourceImp
import ae.gov.fta.maskan.data.remote.MaskanAppApi
import ae.gov.fta.maskan.data.repository.MaskanRepositoryImp
import ae.gov.fta.maskan.domain.MaskanRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
object DataSourcesModule {

    @Provides
    fun provideAppRemoteDataSource(
        apiService: MaskanAppApi
    ): MaskanDataSource = MaskanDataSourceImp(apiService)


    @Provides
    fun provideAppRepository(
        dataSource: MaskanDataSource,
        @IoDispatcher ioDispatcher: CoroutineDispatcher,
    ): MaskanRepository = MaskanRepositoryImp(dataSource, ioDispatcher)

}