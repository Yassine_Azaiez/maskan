package ae.gov.fta.maskan.core.baseUI.activity

import ae.gov.fta.maskan.core.baseUI.fragment.Inflate
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding


abstract class BaseActivity<B : ViewBinding>(private val inflate: Inflate<B>) :
    AppCompatActivity() {

    private var _binding: B? = null
    protected val binding get() = _binding!!

    private val container: ViewGroup by lazy { findViewById<View>(android.R.id.content) as ViewGroup }

    protected lateinit var context: Context

    abstract fun initViewBinding(): B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = initViewBinding()
        context = this
        setContentView(binding.root)
        initView()
       
    }


    /**
     * bind all elements view to use later
     * start on create after init create
     */
    open fun initView() {}

 

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }



}