package ae.gov.fta.maskan.presentation

import ae.gov.fta.maskan.R
import ae.gov.fta.maskan.core.baseUI.activity.BaseActivity
import ae.gov.fta.maskan.databinding.ActivityMainBinding
import android.os.Bundle

class MainActivity : BaseActivity<ActivityMainBinding>(
    ActivityMainBinding::inflate
) {
    override fun initViewBinding(): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)

}