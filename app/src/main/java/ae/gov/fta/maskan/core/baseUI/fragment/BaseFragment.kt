package ae.gov.fta.maskan.core.baseUI.fragment

import ae.gov.fta.maskan.core.baseUI.activity.BaseActivity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding


typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<B : ViewBinding>(private val inflate: Inflate<B>) : Fragment() {

    protected lateinit var baseActivity: BaseActivity<*>

    private var _binding: B? = null
    protected val binding get() = _binding!!

    private var interceptBackClick = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }

    /**
     * bind all elements view to use later
     * start on view created
     */
    abstract fun initView()



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        baseActivity = context as BaseActivity<*>


    }


}